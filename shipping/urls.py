from django.conf.urls import url
from .views import SendProductView, ReverseSendProductView

urlpatterns = [

    url(r'^send/(?P<transaction_id>[^/]+)/$', SendProductView.as_view(), name='send'),
    url(r'^reverse/(?P<transaction_id>[^/]+)/$', ReverseSendProductView.as_view(), name='reverse'),

]
