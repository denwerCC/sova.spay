from django.contrib import admin
from .models import Shipping, ReverseShipping

admin.site.register(Shipping)
admin.site.register(ReverseShipping)
