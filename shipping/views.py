from django.views.generic import CreateView
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from .forms import ShippingForm, ReverseShippingForm
from trade.models import Trade


class SendProductView(CreateView):
    template_name = "shipping/send.html"
    form_class = ShippingForm
    status = 'sent'
    url_name = 'trade:update_as_seller'
    product = None

    def _get_trade(self, transaction_id):
        return get_object_or_404(Trade,
                                 transaction_id=self.kwargs['transaction_id'],
                                 product__user=self.request.user)

    def get(self, request, *args, **kwargs):
        self._get_trade(self.kwargs['transaction_id'])
        return super(SendProductView, self).get(self, request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.product = self._get_trade(self.kwargs['transaction_id'])
        form.instance.product.change_trade_status(self.status)
        self.product = form.instance.product.product
        return super(SendProductView, self).form_valid(form)

    def get_success_url(self):
        return reverse(self.url_name, args=[self.product.spay_id])


class ReverseSendProductView(SendProductView):
    form_class = ReverseShippingForm
    status = 'reverse'
    url_name = 'trade:update_as_buyer'

    def _get_trade(self, transaction_id):
        return get_object_or_404(Trade,
                                 transaction_id=self.kwargs['transaction_id'],
                                 buyer=self.request.user)

    def get_success_url(self):
        return reverse(self.url_name, args=[self.kwargs['transaction_id']])
