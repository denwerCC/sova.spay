# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from trade.models import Trade


class Shipping(models.Model):
    product = models.OneToOneField(Trade)
    shipping_service = models.CharField("Сервіс доставки", max_length=20)
    shipping_id = models.CharField("ID доставки", max_length=40)

    def __unicode__(self):
        return self.shipping_id


class ReverseShipping(models.Model):
    product = models.OneToOneField(Trade)
    shipping_service = models.CharField("Сервіс доставки", max_length=20)
    shipping_id = models.CharField("ID доставки", max_length=40)

    def __unicode__(self):
        return self.shipping_id
