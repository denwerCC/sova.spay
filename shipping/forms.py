# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from .models import Shipping, ReverseShipping


class ShippingForm(forms.ModelForm):
    class Meta:
        model = Shipping
        fields = ('shipping_service', 'shipping_id')


class ReverseShippingForm(forms.ModelForm):
    class Meta:
        model = ReverseShipping
        fields = ('shipping_service', 'shipping_id')
