from django.shortcuts import redirect, get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from product.models import Product
from .forms import AddCardForm, AddBankAccountForm, AddCompanyBankAccountForm, BaseCardForm, \
    AddBankAccountManuallyForm, AddLegalBankAccountManuallyForm, AddLegalBankAccountForm
from .models import Card, BankAccount, BankAccountLegal, ProductCard, ProductBankAccount, ProductBankAccountLegal
from django.views.generic import CreateView, UpdateView
from django.core.urlresolvers import reverse_lazy
from .utils import get_all_user_accounts


def get_initial(model, request):
    initial = model.objects.filter(user=request.user).last()
    return initial if initial else None


@login_required
def checkout_for_payment(request, product_slug):
    product = get_object_or_404(Product, slug=product_slug, user=request.user)
    if request.user.type == "company":
        form_bank = AddLegalBankAccountForm(request.POST or None, user=request.user)
    else:
        form_bank = AddBankAccountForm(request.POST or None, user=request.user)
    form_card = AddCardForm(request.POST or None, user=request.user)
    if request.method == "POST":
        if request.POST.get("payment_method") == "card" and form_card.is_valid():
            form_card.save(product=product)
            return redirect('trade:list')
        elif request.POST.get("payment_method") == "invoice" and form_bank.is_valid():
            form_bank.save(product=product)
            return redirect('trade:list')
        elif request.POST.get("payment_method") not in ("card", "invoice"):
            object_pk = request.POST.get("payment_method").split('-')[1]
            if 'card' in request.POST.get("payment_method"):
                card = get_object_or_404(Card, pk=object_pk, user=request.user)
                ProductCard.add_card(card, product)
            else:
                if request.user.type == 'private':
                    account = get_object_or_404(BankAccount, pk=object_pk, user=request.user)
                    ProductBankAccount.add_card(account, product)
                else:
                    account = get_object_or_404(BankAccountLegal, pk=object_pk, user=request.user)
                    ProductBankAccountLegal.add_card(account, product)
            return redirect('trade:list')
    return render(request, 'cards/card_add.html', {"product": product,
                                                   "form_card": form_card,
                                                   "form_bank": form_bank})


@login_required
def get_all_user_cards(request):
    cards, bank_accounts = get_all_user_accounts(request.user)
    return render(request, 'cards/card_list.html', {'cards': cards, 'bank_accounts': bank_accounts})


class BaseCardView(LoginRequiredMixin, CreateView):
    success_url = reverse_lazy('card:list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(BaseCardView, self).form_valid(form)


class AddCardView(BaseCardView):
    form_class = BaseCardForm
    template_name = 'cards/card_add_manually.html'


class AddAccountView(BaseCardView):
    template_name = 'cards/account_add_manually.html'
    form_class = AddBankAccountManuallyForm


class AddLegalAccountView(AddAccountView):
    form_class = AddLegalBankAccountManuallyForm


class EditCardInfoView(LoginRequiredMixin, UpdateView):
    model = Card
    form_class = BaseCardForm
    template_name = 'cards/card_add_manually.html'
    success_url = reverse_lazy('card:list')

    # fields = ('first_name', 'last_name', 'card_number')

    def get_queryset(self):
        queryset = super(EditCardInfoView, self).get_queryset()
        return queryset.filter(user=self.request.user)


class EditBankAccountInfoView(EditCardInfoView):
    model = BankAccount
    form_class = AddBankAccountManuallyForm
    template_name = 'cards/account_add_manually.html'


class EditLegalBankAccountInfoView(EditBankAccountInfoView):
    model = BankAccountLegal
    form_class = AddLegalBankAccountManuallyForm
