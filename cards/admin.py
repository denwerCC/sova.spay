from django.contrib import admin
from .models import Card, BankAccount, ProductCard

admin.site.register(Card)
admin.site.register(ProductCard)
admin.site.register(BankAccount)

# Register your models here.
