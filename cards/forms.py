# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django import forms
from .models import Card, BankAccount, ProductCard, ProductBankAccount, BankAccountLegal, ProductBankAccountLegal
from .fields import CreditCardField
from .utils import build_data_for_select, get_all_user_accounts_dict

PAYMENT_METHODS = (
    ('card', _('Отримати гроші на нову картку')),
    ('invoice', _('Отримати гроші на новий рахунок'))
)


class BaseCardForm(forms.ModelForm):
    card_number = CreditCardField(label="XXXX")

    def __init__(self, *args, **kwargs):
        super(BaseCardForm, self).__init__(*args, **kwargs)
        for field_name in self.fields:
            field = self.fields.get(field_name)
            field.widget.attrs.update(
                {'placeholder': field.label}
            )
            field.label = ''

    def clean_card_number(self):
        card_number = self.cleaned_data['card_number']
        if not (card_number.isdigit() and len(card_number) == 16):
            raise forms.ValidationError(_("Введіть правильний номер картки"))
        return card_number

    class Meta:
        model = Card
        fields = ('first_name', 'last_name', 'card_number')


class AddCardForm(BaseCardForm):
    model_to_add = ProductCard
    payment_method = forms.ChoiceField(choices=PAYMENT_METHODS)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(AddCardForm, self).__init__(*args, **kwargs)
        if 'payment_method' in self.fields:
            self.fields.get('payment_method').choices = build_data_for_select(self.user) + PAYMENT_METHODS

    def save(self, commit=True, product=None):
        instance = super(AddCardForm, self).save(commit=False)
        instance.user = self.user
        if commit:
            instance.save()
        self.model_to_add.add_card(instance, product)
        return instance


class AddBankAccountForm(AddCardForm):
    field_order = ('payment_method', )
    model_to_add = ProductBankAccount
    payment_method = forms.ChoiceField(choices=PAYMENT_METHODS)
    card_number = None

    class Meta:
        model = BankAccount
        fields = ('name', 'number', 'code', 'mfo', 'legal_address', 'actual_address')


class AddLegalBankAccountForm(AddBankAccountForm):
    model_to_add = ProductBankAccountLegal

    class Meta:
        model = BankAccountLegal
        fields = ('name', 'number', 'code', 'mfo', 'legal_address', 'actual_address')


class AddCompanyBankAccountForm(AddBankAccountForm):
    field_order = ()
    payment_method = None


class AddBankAccountManuallyForm(BaseCardForm):
    card_number = None

    class Meta:
        model = BankAccount
        fields = ('name', 'number', 'code', 'mfo', 'legal_address', 'actual_address')


class AddLegalBankAccountManuallyForm(BaseCardForm):
    card_number = None

    class Meta:
        model = BankAccountLegal
        fields = ('name', 'number', 'code', 'mfo', 'legal_address', 'actual_address')
