from django import forms


class CreditCardWidget(forms.MultiWidget):
    def __init__(self, attrs=None):
        widgets = [forms.TextInput(attrs={'size': 4, 'maxlength': 4}), ] * 4
        super(CreditCardWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value[i:i + 4] for i in xrange(0, len(value), 4)]
        return [None]*4


class CreditCardField(forms.MultiValueField):
    def __init__(self, required=True, widget=None, label=None, initial=None):
        fields = (forms.CharField(max_length=4),) * 4
        widget = CreditCardWidget()
        super(CreditCardField, self).__init__(fields, required,
                                              widget, label, initial)

    def compress(self, data_list):
        if data_list:
            return "".join(data_list)
        return None
