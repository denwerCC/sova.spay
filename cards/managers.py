from django.db import models


class WalletQuerySet(models.QuerySet):
    def user(self, user):
        return self.filter(user=user)

