from django.conf.urls import url
from .views import checkout_for_payment, get_all_user_cards, AddCardView, AddAccountView, AddLegalAccountView, \
    EditCardInfoView, EditBankAccountInfoView, EditLegalBankAccountInfoView

urlpatterns = [
    # url(r'^add/(?P<product_slug>[-\w]+)/$', AddCard.as_view(), name='add_old'),
    url(r'^add/(?P<product_slug>[-\w]+)/$', checkout_for_payment, name='add'),
    # url(r'^add_bank/(?P<product_slug>[-\w]+)/$', company_payment, name='add_bank_account'),
    url(r'^list/$', get_all_user_cards, name='list'),
    url(r'^new_card/$', AddCardView.as_view(), name='card_add_manually'),
    url(r'^new_account/$', AddAccountView.as_view(), name='account_add_manually'),
    url(r'^new_legal_account/$', AddLegalAccountView.as_view(), name='legal_account_add_manually'),
    # url(r'^get_payment_form/(?P<form_type>[-\w]+)/$', get_payment_form, name='get_payment_form'),
    url(r'^edit_card/(?P<pk>[-\w]+)/$', EditCardInfoView.as_view(), name='edit_card_info'),
    url(r'^edit_bank_account/(?P<pk>[-\w]+)/$', EditBankAccountInfoView.as_view(), name='edit_bank_account_info'),
    url(r'^edit_legal_bank_account/(?P<pk>[-\w]+)/$', EditLegalBankAccountInfoView.as_view(), name='edit_legal_bank_account_info'),
]
