# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings
from product.models import Product
from .managers import WalletQuerySet


class Card(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    card_number = models.CharField(max_length=16)

    objects = models.Manager()
    wallet = WalletQuerySet.as_manager()

    @property
    def number_to_show(self):
        return self.card_number[:4] + ' **** **** ' + self.card_number[-4:]

    @property
    def cut_numbers_for_select(self):
        last_4_numbers = 'Card ' + self.number_to_show
        key = 'card-' + str(self.pk)
        data_for_select = (key, last_4_numbers)
        return data_for_select

    # def save(self, *args, **kwargs):
    #     try:
    #         card = Card.objects.get(card_number=self.card_number, user=self.user)
    #     except Card.DoesNotExist:
    #         super(Card, self).save(*args, **kwargs)
    #     else:
    #         return

    def __unicode__(self):
        return "%s" % self.card_number


class ProductCard(models.Model):
    card = models.ForeignKey(Card)
    product = models.OneToOneField(Product, related_name="product_card")

    @classmethod
    def add_card(cls, card, product):
        return cls.objects.update_or_create(product=product, defaults={"card": card})

    def __unicode__(self):
        return "%s-%s" % (self.card, self.product)


class BankAccount(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    number = models.BigIntegerField(_("Розрахунковий рахунок"))
    code = models.BigIntegerField(_("Код ЄДРПОУ (ЗКПО)"))
    mfo = models.IntegerField(_("Код банку (МФО)"))
    name = models.CharField(_("Ім'я власника рахунку"), max_length=128)
    actual_address = models.CharField(_("Фактична адреса"), max_length=512)
    legal_address = models.CharField(_("Юридична адреса"), max_length=256)

    objects = models.Manager()
    wallet = WalletQuerySet.as_manager()

    @property
    def number_to_show(self):
        return str(self.number)[:4] + ' **** **** ' + str(self.number)[-4:]

    @property
    def cut_numbers_for_select(self):
        last_4_numbers = 'Account ' + self.number_to_show
        key = 'invoice-' + str(self.pk)
        data_for_select = (key, last_4_numbers)
        return data_for_select


class ProductBankAccount(models.Model):
    bank_account = models.ForeignKey(BankAccount)
    product = models.OneToOneField(Product, related_name="product_bank_account")

    @classmethod
    def add_card(cls, card, product):
        return cls.objects.update_or_create(product=product, defaults={"bank_account": card})


class BankAccountLegal(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    number = models.BigIntegerField(_("Розрахунковий рахунок"))
    code = models.BigIntegerField(_("Код ЄДРПОУ (ЗКПО)"))
    mfo = models.IntegerField(_("Код банку (МФО)"))
    name = models.CharField(_("Найменування організації"), max_length=128)
    actual_address = models.CharField(_("Фактична адреса"), max_length=512)
    legal_address = models.CharField(_("Юридична адреса"), max_length=256)

    objects = models.Manager()
    wallet = WalletQuerySet.as_manager()

    @property
    def number_to_show(self):
        return str(self.number)[:4] + ' **** **** ' + str(self.number)[-4:]

    @property
    def cut_numbers_for_select(self):
        last_4_numbers = 'Account ' + self.number_to_show
        key = 'invoice-' + str(self.pk)
        data_for_select = (key, last_4_numbers)
        return data_for_select


class ProductBankAccountLegal(models.Model):
    bank_account = models.ForeignKey(BankAccountLegal)
    product = models.OneToOneField(Product, related_name="product_bank_account_legal")

    @classmethod
    def add_card(cls, card, product):
        return cls.objects.update_or_create(product=product, defaults={"bank_account": card})
