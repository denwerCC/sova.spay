from .models import Card, BankAccount, BankAccountLegal
from itertools import chain


def get_all_user_accounts(user):
    cards = Card.wallet.user(user)
    if user.type == "private":
        bank_accounts = BankAccount.wallet.user(user)
    else:
        bank_accounts = BankAccountLegal.wallet.user(user)
    return cards, bank_accounts


def build_data_for_select(user):
    cards, bank_accounts = get_all_user_accounts(user)
    return tuple([data.cut_numbers_for_select for data in list(chain(cards, bank_accounts))])


def get_all_user_accounts_dict(user):
    cards = Card.wallet.user(user)
    if user.type == "private":
        bank_accounts = BankAccount.wallet.user(user)
    else:
        bank_accounts = BankAccountLegal.wallet.user(user)
    return list(cards.values()), list(bank_accounts.values())
