from django.contrib import admin
from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    search_fields = ('name', 'email', 'phone')
    list_display = ('email', 'name', 'phone', 'is_staff')
    list_filter = ('is_staff',)
