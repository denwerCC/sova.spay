from django.contrib.auth.forms import UserCreationForm
from django.forms import RadioSelect
from .models import User


class SpayUserCreationForm(UserCreationForm):
    def signup(self, request, user):
        user.email = self.cleaned_data['email']
        user.type = self.cleaned_data['type']
        user.save()

    class Meta:
        model = User
        fields = ('email', 'type')
        widgets = {
            'type': RadioSelect(),
        }
