from django.conf.urls import url
from .views import UpdateUserView, UserDetailsView, ajax_get_user_by_email, ajax_login

urlpatterns = [
    url(r'^profile/$', UpdateUserView.as_view(), name='update'),
    url(r'^details/(?P<pk>[-\w]+)/$', UserDetailsView.as_view(), name='details'),
    url(r'^exists/$', ajax_get_user_by_email, name='exists'),
    url(r'^ajax-login/$', ajax_login, name='ajax_login')

]
