# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from django.views.generic import UpdateView, DetailView
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.translation import ugettext_lazy as _
from .models import User
from .serializers import PublibUserSerializer


@api_view(['POST'])
@permission_classes((AllowAny,))
def ajax_get_user_by_email(request):
    return Response(
        {"exists": get_user_model().objects.filter(email=request.POST.get('email')).exists()}
    )


@api_view(['POST'])
@permission_classes((AllowAny,))
def ajax_login(request):
    user = authenticate(
        email=request.POST.get('email'),
        password=request.POST.get('password')
    )
    if user is not None:
        login(request, user)
        return Response({"user": PublibUserSerializer(user).data})
    else:
        return Response({"error": _("Невірний пароль")}, status=401)


class UpdateUserView(LoginRequiredMixin, UpdateView):
    model = User
    template_name = "user_profile.html"
    fields = ('email', 'name', 'phone')

    def get_object(self, queryset=None):
        return get_object_or_404(self.model, spay_id=self.request.user.spay_id)

    def get_success_url(self):
        return reverse_lazy("user:update")

    def get_context_data(self, **kwargs):
        context = super(UpdateUserView, self).get_context_data(**kwargs)
        return context


class UserDetailsView(DetailView):
    template_name = "user_details_view.html"
    context_object_name = 'user'
    model = User
