from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from rest_framework.authtoken import views
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .api import UserViewSet

router = DefaultRouter()
router.register(r'user', UserViewSet)

urlpatterns = [
                  url(r'^api-login-web-token/', obtain_jwt_token),
                  url(r'^api-refresh-web-token/', refresh_jwt_token),
                  url(r'^api-verify-web-token/', verify_jwt_token),
                  url(r'^login/', views.obtain_auth_token),
              ] + router.urls
