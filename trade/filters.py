# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django_filters import FilterSet, ChoiceFilter
from product.models import Product, NEW_PRODUCT_STATUS_NAME
from .models import Trade, STATUS


class ProductFilter(FilterSet):
    AVAILABLE = (
        ('available', NEW_PRODUCT_STATUS_NAME),
    )

    ROLE = (
        ('buyer', _('Покупець')),
        ('seller', _('Продавець'))
    )

    product_status_filter = ChoiceFilter(method='status_filter', choices=AVAILABLE + STATUS)
    role = ChoiceFilter(method='role_filter', choices=ROLE)

    class Meta:
        model = Product
        fields = {'spay_id': ['exact', ],
                  'name': ['exact', 'icontains'],
                  'price': ['exact', ],
                  'created_date': ['date__lt', 'date__gt']}

    def status_filter(self, queryset, name, value):
        return queryset.filter(
            pk__in=[x.pk for x in queryset if x.status == value]
        )

    def role_filter(self, queryset, name, value):
        if value == "seller":
            return queryset.filter(user=self.request.user)
        else:
            return queryset.exclude(user=self.request.user)
