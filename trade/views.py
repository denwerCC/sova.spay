# -*- coding: utf-8 -*-
from django.shortcuts import redirect
from django.contrib import messages
from django.views.generic import DetailView, ListView, RedirectView
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from product.models import Product
from django.core.urlresolvers import reverse_lazy
from .models import Trade
from .filters import ProductFilter


class UsersTradeList(LoginRequiredMixin, ListView):
    model = Product
    template_name = "user_transactions.html"
    context_object_name = 'products'

    def get_queryset(self):
        return self.model.users.all_products(self.request.user)

    def get_context_data(self, **kwargs):
        context = super(UsersTradeList, self).get_context_data(**kwargs)
        context['filter'] = ProductFilter(
            self.request.GET,
            queryset=self.get_queryset(),
            request=self.request
        )
        return context


class BaseBuyerTradeStatusChange(LoginRequiredMixin, DetailView):
    """Base View for status change. Make sure You are doing right things changing it"""
    slug_field = 'transaction_id'
    slug_url_kwarg = 'transaction_id'
    model = Trade
    new_status = 'sent'
    redirect_to = "trade:list"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return redirect(self.redirect_to)

    def get_queryset(self):
        return super(BaseBuyerTradeStatusChange, self).get_queryset().filter(buyer=self.request.user)

    def get_object(self, queryset=None, change=True):
        trade = super(BaseBuyerTradeStatusChange, self).get_object(queryset)
        trade.change_trade_status(self.new_status)
        return trade


class NotRelevantTradeChange(BaseBuyerTradeStatusChange):
    new_status = "not_relevant"


class PayTradeChange(BaseBuyerTradeStatusChange):
    new_status = "payed"
    redirect_to = 'https://www.liqpay.com/'


class CancelTradeChange(BaseBuyerTradeStatusChange):
    new_status = "cancel"

    def test_func(self):
        return self.get_object(change=False).product.user == self.request.user


class DoneTradeChange(BaseBuyerTradeStatusChange):
    new_status = "done"


class VarianceTradeChange(BaseBuyerTradeStatusChange):
    new_status = "variance"


class ReverseTradeChange(BaseBuyerTradeStatusChange):
    new_status = "reverse"


class CancelPayment(BaseBuyerTradeStatusChange):
    new_status = "cancel_payment"


class UpdateTradeSellerView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Product
    template_name = "product/product_change_status_seller.html"
    context_object_name = 'product'
    slug_field = 'spay_id'
    slug_url_kwarg = 'spay_id'
    raise_exception = True
    success_url = reverse_lazy('trade:list')
    template_name_folder = 'status/seller/%s.html'

    def get_context_data(self, **kwargs):
        context = super(UpdateTradeSellerView, self).get_context_data(**kwargs)
        context['buttons_template_name'] = self.template_name_folder % self.object.status
        return context

    def test_func(self):
        return self.get_object().user == self.request.user


class UpdateTradeBuyerView(UpdateTradeSellerView):
    model = Trade
    template_name = "product/product_change_status_buyer.html"
    context_object_name = 'trade'
    slug_field = 'transaction_id'
    slug_url_kwarg = 'transaction_id'
    template_name_folder = 'status/%s.html'

    def get_context_data(self, **kwargs):
        context = super(UpdateTradeBuyerView, self).get_context_data(**kwargs)
        context["product"] = self.object.product
        # context['buttons_template_name'] = self.template_name_folder % self.object.status
        return context

    def test_func(self):
        return self.get_object().buyer == self.request.user


class SendTradeChange(BaseBuyerTradeStatusChange):
    new_status = 'sent'

    def get_queryset(self):
        return super(BaseBuyerTradeStatusChange, self).get_queryset().filter(product__user=self.request.user)


class RejectGetTradeChange(SendTradeChange):
    new_status = "reject_get"
