# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from .models import Trade
from model_utils import Choices

STATUS = Choices("Товар отправлен",
                 "Отменено - возвращено",
                 "Отменено - не актуальная сделка")

STATUS_BUY = Choices("Сделка состоялась",
                     "Отменено - несоответствие",
                     "Отменено - обратная отправка",
                     "Отменено - не актуально покупателю")


class ChangeTradeStatusBySellerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ChangeTradeStatusBySellerForm, self).__init__(*args, **kwargs)
        self.fields['status'].choices = STATUS

    class Meta:
        model = Trade
        fields = ('status', 'shipping_service', 'shipping_id')


class ChangeTradeStatusByBuyerForm(ChangeTradeStatusBySellerForm):
    def __init__(self, *args, **kwargs):
        super(ChangeTradeStatusByBuyerForm, self).__init__(*args, **kwargs)
        self.fields['status'].choices = STATUS_BUY
