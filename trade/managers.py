from datetime import datetime, timedelta
from django.db import models


class TradeQuerySet(models.QuerySet):
    def buyer(self, user):
        return self.filter(buyer=user)

    def seller(self, user):
        return self.filter(product__user=user)

    def both(self, user):
        return self.filter(buyer=user | models.Q(product__user=user)).select_related()

    def booked_two_hours_ago(self):
        td = datetime.now() - timedelta(minutes=5)
        return self.filter(created__lt=td, status='booked')
