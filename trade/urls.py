from django.conf.urls import url
from .views import (
    UpdateTradeSellerView, NotRelevantTradeChange, DoneTradeChange,
    UpdateTradeBuyerView, UsersTradeList, PayTradeChange, CancelPayment,
    VarianceTradeChange, ReverseTradeChange, SendTradeChange, RejectGetTradeChange
)

urlpatterns = [
    # url(r'^new/(?P<spay_id>[\-\w]+)/$', create_trade, name='create'),

    url(r'^list/$', UsersTradeList.as_view(), name='list'),

    url(r'^update_seller/(?P<spay_id>[^/]+)/$', UpdateTradeSellerView.as_view(), name='update_as_seller'),
    url(r'^update_buyer/(?P<transaction_id>[^/]+)/$', UpdateTradeBuyerView.as_view(), name='update_as_buyer'),

    url(r'^not_relevant_buyer/(?P<transaction_id>[^/]+)/$', NotRelevantTradeChange.as_view(), name='not_relevant'),
    url(r'^receive_buyer/(?P<transaction_id>[^/]+)/$', DoneTradeChange.as_view(), name='done'),
    url(r'^pay_buyer/(?P<transaction_id>[^/]+)/$', PayTradeChange.as_view(), name='pay'),
    url(r'^cancel_payment_buyer/(?P<transaction_id>[^/]+)/$', CancelPayment.as_view(), name='cancel_payment'),
    url(r'^variance_buyer/(?P<transaction_id>[^/]+)/$', VarianceTradeChange.as_view(), name='variance'),
    url(r'^reverse_buyer/(?P<transaction_id>[^/]+)/$', ReverseTradeChange.as_view(), name='reverse'),

    url(r'^send_seller/(?P<transaction_id>[^/]+)/$', SendTradeChange.as_view(), name='send'),
    url(r'^reject_get_seller/(?P<transaction_id>[^/]+)/$', RejectGetTradeChange.as_view(), name='reject_get'),

]
