from .models import Trade
from celery.task import periodic_task
from datetime import timedelta


@periodic_task(run_every=timedelta(seconds=60))
def revert_product_to_new():
    for trade in Trade.users.booked_two_hours_ago():
        trade.change_trade_status('not_relevant')
