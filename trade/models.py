# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import base64
import uuid
from model_utils.models import TimeStampedModel
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import PermissionDenied
from model_utils import Choices
from .managers import TradeQuerySet

SHIPPING = Choices("Нова Пошта",
                   "DHL")


def generate_transaction_id():
    return base64.b64encode(uuid.uuid4().bytes).replace('=', '').replace('/', '')


STATUS = (
    ("booked", _("Заброньований")),
    ("payed", _("Оплачено, Очікує виконання")),
    ("sent", _("Товар отправлен")),
    ("done", _("Сделка состоялась")),
    ("variance", _("Скасовано - несоответствие")),
    ("reverse", _("Скасовано - зворотня відправка")),
    ("reject_get", _("Скасовано - отримано")),
    ("cancel_payment", _("Скасовано оплату покупцем")),
    ("not_relevant", _("Скасовано - не актуально покупцю")),
    ("cancel", _("Скасовано  - не актуальна угода"))
)

SWITCH_STATUS = {
    'available': ('booked', 'payed', 'cancel'),
    'booked': ('payed', 'not_relevant', 'cancel'),
    'payed': ('sent', 'cancel', 'cancel_payment'),
    'sent': ('done', 'variance', 'done'),
    'done': (),
    'variance': ('reverse',),
    'reverse': ('reject_get',),
    'cancel': (),
}

AUTO_NEW_STATUS = ('cancel_payment', 'not_relevant')


class Trade(TimeStampedModel):
    buyer = models.ForeignKey(settings.AUTH_USER_MODEL,
                              to_field="spay_id",
                              verbose_name="Покупець",
                              related_name='buyer',
                              blank=True,
                              null=True)
    transaction_id = models.CharField("Transaction id",
                                      max_length=22,
                                      unique=True,
                                      default=generate_transaction_id)
    status = models.CharField("Статус", default="booked", choices=STATUS, max_length=32)
    product = models.ForeignKey('product.Product',
                                verbose_name="Товар", )

    objects = models.Manager()
    users = TradeQuerySet.as_manager()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.product.product_status = 'in_deal'
            self.product.save()
        super(Trade, self).save(*args, **kwargs)

    def change_trade_status(self, status):
        if status in SWITCH_STATUS.get(self.status):
            self.status = status
            self.save()
            if self.status in AUTO_NEW_STATUS:
                self.product.product_status = "available"
                self.product.save()
        else:
            raise PermissionDenied

    @property
    def seller(self):
        return self.product.user

    def __unicode__(self):
        return u"{0}-{1}".format(self.product.name, self.transaction_id)
