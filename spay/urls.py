from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings

urlpatterns = i18n_patterns(
    url(r'', include('core.urls')),
    url(r'^admin/', admin.site.urls),

    # url(r'^api/v1/', include('spay.api_urls_v1', namespace='api-v1')),
    # url(r'^docs/', include('rest_framework_docs.urls')),

    url(r'^product/', include('product.urls', namespace="product")),
    url(r'^trade/', include('trade.urls', namespace="trade")),
    url(r'^card/', include('cards.urls', namespace="card")),
    url(r'^shipping/', include('shipping.urls', namespace="shipping")),

    url(r'^user/', include('suser.urls', namespace="user")),
    url(r'^accounts/', include('allauth.urls')),

    url(r'^message/', include('chat.urls', namespace='chat')),

)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += i18n_patterns(
        url(r'^rosetta/', include('rosetta.urls')),
    )
