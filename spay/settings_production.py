from settings import *

STATIC_ROOT = os.path.join(BASE_DIR, 'collected_static')

DEBUG = True

ALLOWED_HOSTS = (
    '127.0.0.1',
    'localhost',
    'spay.byteside.co',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'spay_db',
        'USER': 'spay',
        'PASSWORD': 'xan8nDqWJsCM',
        'HOST': 'localhost',
        'PORT': '',
    }
}

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "CONFIG": {
            "hosts": [("127.0.0.1", 6379)],
        },
        "ROUTING": "spay.routing.channel_routing",
    },
}
