from django.conf.urls import url, include

urlpatterns = [
    url(r'^auth/', include('suser.api_urls')),
    url(r'^product/', include('product.api_urls'))

]
