from channels.routing import route
from chat.consumers import ws_add, ws_message, ws_disconnect, msg_consumer, ws_new_message_count, \
    ws_new_message_count_disconnect
from core.utils import send_email_consumer

channel_routing = [
    route("websocket.connect", ws_add, path=r"^/chat/(?P<product>[-\w]+)/(?P<user_id>[-\w]+)/$"),
    route("websocket.connect", ws_new_message_count, path=r"^/new_messages/$"),
    route("websocket.receive", ws_message),
    route("websocket.disconnect", ws_disconnect, path=r"^/chat/(?P<product>[-\w]+)/(?P<user_id>[-\w]+)/$"),
    route("websocket.disconnect", ws_new_message_count_disconnect, path=r"^/new_messages/$"),
    route("chat-messages", msg_consumer),
    route("send-email", send_email_consumer),

]
