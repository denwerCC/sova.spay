import os
import datetime

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_hij-n(o48g)%(8=byqcxpi+!hj2e)1ne4g)-*5*%+f)@+2(q9'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = [
    "suit",

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.humanize',

    "core",
    "suser",
    "product",
    "trade",
    "chat",
    "cards",
    "shipping",

    'allauth',
    'allauth.account',

    # 'allauth.socialaccount',
    # 'allauth.socialaccount.providers.facebook',
    # 'allauth.socialaccount.providers.vk',
    # 'rest_framework',
    # 'rest_framework.authtoken',
    # 'rest_framework_docs',

    'crispy_forms',
    'django_filters',
    'sorl.thumbnail',
    'rosetta',
    # 'countries_plus',

    'channels',
]

########## CELERY
INSTALLED_APPS += ['taskapp.celery.CeleryConfig',]

SITE_ID = 1

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

]

ROOT_URLCONF = 'spay.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'chat.context_processors.messages_count'
            ],
        },
    },
]

WSGI_APPLICATION = 'spay.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators


# account
AUTH_USER_MODEL = "suser.User"

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# https://django-allauth.readthedocs.io/en/latest/

AUTHENTICATION_BACKENDS = (
    'allauth.account.auth_backends.AuthenticationBackend',
)

ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_USER_EMAIL_FIELD = 'email'
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_EMAIL_VERIFICATION = 'none'
ACCOUNT_EMAIL_SUBJECT_PREFIX = "[Spay]"
LOGIN_REDIRECT_URL = '/user/profile/'
SOCIALACCOUNT_AUTO_SIGNUP = True
ACCOUNT_SIGNUP_FORM_CLASS = 'suser.forms.SpayUserCreationForm'

# SOCIALACCOUNT_PROVIDERS = \
#     {'facebook':
#          {'METHOD': 'js_sdk ',
#           'SCOPE': ['email', 'public_profile'],
#           'AUTH_PARAMS': {'auth_type': 'reauthenticate'},
#           'EXCHANGE_TOKEN': True,
#           'VERIFIED_EMAIL': False,
#           'VERSION': 'v2.4'}
#      }

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'uk'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)
LANGUAGES = (
    ('uk', 'Ukraine'),
    ('en', 'English'),
    ('pl', 'Poland'),
    ('ru', 'Russian')
)

# Static files (CSS, JavaScript, Images) https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),

]
# media
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

# email
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.zoho.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'uspay@byteside.co'
EMAIL_HOST_PASSWORD = 'uSpayuSpay01;'
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

# crispy forms

CRISPY_TEMPLATE_PACK = 'bootstrap3'

# sorl
THUMBNAIL_DEBUG = True
THUMBNAIL_QUALITY = 80
THUMBNAIL_PROGRESSIVE = False
THUMBNAIL_PRESERVE_FORMAT = True

# suit
SUIT_CONFIG = {
    'ADMIN_NAME': 'Spay Safe payments',
    'SEARCH_URL': '',
    'MENU': INSTALLED_APPS + [{'label': 'Translation', 'url': '/rosetta/'}]
}

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        # 'rest_framework.authtoken',
    ),
}

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=31)
}

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgiref.inmemory.ChannelLayer",
        "ROUTING": "spay.routing.channel_routing",
    },
}

CELERY_ALWAYS_EAGER = True
