from settings import *

STATIC_ROOT = os.path.join(BASE_DIR, 'collected_static')

ALLOWED_HOSTS = ["*", ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'spay_db',
        'USER': 'spay',
        'PASSWORD': 'xan8nDqWJsCM',
        'HOST': 'localhost',
        'PORT': '',
    }
}