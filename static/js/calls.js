$(document).ready(function () {
    var phone_settings = {
        preferredCountries: ["ua", 'pl'],
        //separateDialCode: true,
        //autoHideDialCode: false,
        nationalMode: false,
        initialCountry: "auto",
        geoIpLookup: function (callback) {
            $.get('http://ipinfo.io', function () {
            }, "jsonp").always(function (resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        utilsScript: "/static/phone_field/js/utils.js"
    }
    $("#id_phone").intlTelInput(phone_settings);
    $("#id_phone").mask("+00000000000000000");
    $("#id_phone_buyer").intlTelInput(phone_settings);
    $("#id_phone_buyer").mask("+00000000000000000");
    function notify_about_msg(title, message, url) {
        $.notify({
                title: title,
                message: message,
                url: url
            },
            {
                type: 'custom',
                delay: 5000,
                newest_on_top: true,
                placement: {
                    from: 'bottom',
                    align: 'right'
                },
                animate: {
                    enter: 'animated zoomInDown',
                    exit: 'animated zoomOutUp'
                },
                template: '<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span class="glyphicon glyphicon-envelope"></span><span data-notify="title">{1}</span>' +
                '<span data-notify="message">{2}</span>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
            }
        );
    }

    function add_count(div) {
        div.html(parseInt((div.html()) || 0) + 1);
    }

    var chat_header = $(".js-notify")
    if (chat_header) {
        var new_chatsock = new WebSocket(ws_scheme + '://' + window.location.host + "/new_messages/");
        new_chatsock.onmessage = function (message) {
            var data = JSON.parse(message.data);
            var messages_count = $("#messages_count")
            var theme_messages_count = $("#" + data.theme)
            add_count(messages_count)
            console.log(theme_messages_count)
            add_count(theme_messages_count)
            if (chat_header.attr('chat-list-url') == window.location.pathname) {
                notify_about_msg(data.author, data.text, data.url)
            }
        };
    }
});