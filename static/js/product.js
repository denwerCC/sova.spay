$(document).ready(function () {
    var typingTimer;
    var doneTypingInterval = 1000;
    var email_input = $('#id_email');
    email_input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });
    email_input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    email_input.change(function () {
        doneTyping()
    });
    if (email_input.length) {
        if (!$(".js-notify").length) {
            doneTyping()
        }
        ;
    }
    ;
    function doneTyping() {
        var password_div = $("#div_id_password").parent()
        var email_div = $("#div_id_email").parent()
        var login_btn_div = $("#div_id_login").parent()
        $.ajax({
            type: "POST",
            url: email_input.attr('ajax-url'),
            success: function (msg) {
                console.log(msg)
                if (msg.exists == true) {
                    email_div.after(password_div.show());
                    password_div.after(login_btn_div.show());
                    $("#div_id_phone").hide().parent().hide();
                    $("#div_id_user_type").hide().parent().hide();
                }
                else {
                    password_div.hide();
                    $("#div_id_phone").show().parent().show();
                    login_btn_div.hide()
                    $("#div_id_user_type").show().parent().show();
                }
            },
            error: function (msg) {
                console.log("spme error" + msg)
            },
            data: {"email": email_input.val()}
        })
        ;
    };
    $("#login").on('click', function () {
        var phone_field = $("#id_phone")
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: $(this).attr('ajax-url'),
            success: function (msg) {
                $("#id_password").hide().parents().slice(0, 3).hide();
                phone_field.val(msg.user.phone)
                $("#div_id_phone").show().parent().show();
                $("#login").hide().parents().slice(0, 3).hide();
                $("#div_id_user_type").hide().parent().hide();
                $("#id_email").prop('readonly', 1)
                $('input[name="csrfmiddlewaretoken"]').val($.cookie('csrftoken'))
                $('#id_regirtered').removeClass('hidden');
                $('#id_signin').addClass('hidden');
                email_input.next().remove();
                email_input.removeClass("has-error");
                phone_field.removeClass("has-error");
            },
            error: function (msg) {
                $("#div_id_password").addClass("has-error")
                console.log(msg)
                $("#id_password").next().remove()
                $("#id_password").after("<p class='help-block'><strong>" + msg.responseJSON.error + "</strong></p>");
            },
            data: {
                "email": email_input.val(),
                "password": $("#id_password").val()
            }
        })
        ;
    });
});
