$(document).ready(function () {
    var message_list = $(".message-list")
    var chat = $("#chat");
    //Message list
    message_list.niceScroll({
        cursorcolor: "lightgrey",
        cursorborderradius: "20px",
        cursorfixedheight: 40,
        horizrailenabled: false,
    });
    message_list.scrollTop(message_list[0].scrollHeight)

    var chatsock = new ReconnectingWebSocket(ws_scheme + '://' + window.location.host +
        "/chat/" + chat.attr('product-sign') + chat.attr('user-sign'));
    chatsock.onmessage = function (message) {
        var data = JSON.parse(message.data);
        if (data.author == chat.attr('current-user-sign')) {
            message_html = $($.parseHTML(data.html));
            message_html.find('.message').addClass("author");
            message_html.find('.user-name').remove();
        }
        else {
            message_html = data.html
        }
        message_list.append(message_html)
        message_list.scrollTop(message_list[0].scrollHeight)
    };

    chat.on("submit", function (event) {
        var message_input = $('#message')
        var message = message_input.val();
        if (message) {
            chatsock.send(message);
            message_input.val('').focus();
        }
        return false;
    });
});
