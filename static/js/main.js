$(document).ready(function () {
        //type of protocol
        ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
        //copy to clipboard
        new Clipboard('.copy-button');
        //end
        var image_input;
        var spay_id_input = $("#tbSpayId");
        var price_input = $("#tbPrice");
        var spay_id_tooltip = $('[data-toggle="tooltip"]');
        //card
        $("#id_card_number_0").numeric();
        $("#id_card_number_1").numeric();
        $("#id_card_number_2").numeric();
        $("#id_card_number_3").numeric();
        //sc
        $("textarea").niceScroll({
            cursorcolor: "lightgrey",
            cursorborderradius: "20px",
            cursorfixedheight: 10
        });
        var transaction_type = 'sell';
        var product_type = 'goods';
        $("#entity.custom-select .options li a").on('click', function () {
            transaction_type = $(this).attr('data-value');
            if (transaction_type == "buy") {
                $("#action_input")[0].value = "buy";
                spay_id_input.css('visibility', 'visible');
                $("#question_mark").css('visibility', 'visible');
                $("#ddlItem").addClass('disabled');
                price_input.prop('disabled', 1);
                $("#option-buy").insertBefore("#option-sell");
                price_input.tooltip('destroy');
                spay_id_tooltip.tooltip();
            }
            else if (transaction_type == "sell") {
                $("#action_input")[0].value = "sell";
                spay_id_input.css('visibility', 'hidden');
                $("#question_mark").css('visibility', 'hidden');
                $("#ddlItem").removeClass('disabled');
                price_input.prop('disabled', 0);
                $("#option-sell").insertBefore("#option-buy");
                spay_id_input.tooltip('destroy');
                spay_id_tooltip.tooltip('destroy');
            }
        });
        $("#ddlItem.custom-select .options li a").on('click', function () {
            product_type = $(this).attr('data-value');
            $('#type_input')[0].value = product_type;
            if (product_type == 'goods') {
                $("#option-goods").insertBefore("#option-service");

            } else if (product_type == 'service') {
                $("#option-service").insertBefore("#option-goods");
            }
        });

        $("#btnGetStarted").on('click', function () {
            event.preventDefault();
            if (transaction_type == 'sell' && price_input[0].value == "") {
                showTooltip(price_input, $('input[name=price]').attr('data-title'));
                return false
            } else if (transaction_type == 'buy' && spay_id_input[0].value == "") {
                showTooltip(spay_id_input, $('input[name=spay_id]').attr('data-title'));
                return false
            } else {
                $.ajax({
                    type: 'POST',
                    url: window.location,
                    data: $('form').serialize(),
                    success: function (msg) {
                        if (msg.status == 'error') {
                            showTooltip(spay_id_input, msg.text);
                        } else if (msg.status == 'sold') {
                            showTooltip(spay_id_input, msg.text);
                        } else if (msg.status == 'OK') {
                            window.location = msg.url;
                        }
                    }
                });
            }
        });

        $("#cbTerms").on('click', function () {
            var btn_proc = $(".js-accept");
            if ($(this).prop("checked") == true) {
                btn_proc.prop("disabled", false)
            }
            else {
                btn_proc.prop("disabled", 1)
            }
        });

        $(":file").change(function () {
            image_input = $(this);
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });

        function imageIsLoaded(e) {
            var image_back = image_input.closest("div.js-background");
            if (image_back.attr('class').indexOf("item-upload-image ") !== -1) {
                $(".move").removeClass("hidden");
            }
            image_back.css('background-image', 'url(' + e.target.result + ')');
            image_input.next('.js-opacity-0').css('opacity', '0.0');
            image_back.parent().next().find(".js-background").removeClass("hidden");
        }

        price_input.numeric();

        $(".js-open").click(function () {
            $(".product-images").toggleClass("hidden");
        });

        $('.js-change :input').change(function () {
            $('#filter_form').submit();
        });

    }
);

$('#tbPrice').focus(function () {
    $(this).tooltip('destroy');
});

$('#tbSpayId').focus(function () {
    $(this).tooltip('destroy');
});
function showTooltip(selector, text) {
    selector.tooltip('destroy');
    setTimeout(function () {
        selector.tooltip({trigger: 'manual', title: text}).tooltip('show');
    }, 200);
}



