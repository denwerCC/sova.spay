$(document).ready(function () {
    /iP/i.test(navigator.userAgent) && $('*').css('cursor', 'pointer');

    $(document).on('click', function (e) {
        var openedOptions = $('.options:visible');
        if (openedOptions.length > 0) {
            openedOptions.hide();
            openedOptions.parent().removeClass('arrow-up');
        }

    });

    $(document).on('click', '.custom-select:not(.disabled)', function (e) {
        e.stopPropagation();
        var openedOptions = $('.options:visible');
        var options = $(this).find('.options');
        if (openedOptions.length > 0 && options.is(':hidden')) {
            openedOptions.hide();
        }
        $(this).toggleClass('arrow-up');
        options.toggle();
    });

    $(document).on('click', '.custom-select .options li', function (e) {
        e.stopPropagation();
        var target = $(e.target);
        var select = target.closest('.custom-select');
        if (target.data('value') != null) {
            select.removeClass('arrow-up')
            console.log('SELECTED VALUE: ' + target.data('value'));
            var selected = select.find('.selected');
            var options = select.find('.options');

            selected.text(target.text());
            if (target.data('image') != null) {
                selected.css('background-image', target.data('image'));
            }
            options.toggle();
        }
    });
});