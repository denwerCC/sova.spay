# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.crypto import get_random_string
from django.core.exceptions import PermissionDenied
from django.db import models
from django.utils.translation import ugettext_lazy as _
from slugify import slugify
from django.core.urlresolvers import reverse
from django.conf import settings
from sorl.thumbnail import ImageField
from decimal import Decimal
from .managers import ProductQuerySet


def get_spay_id():
    return get_random_string(length=6).upper()


# class Category(models.Model):
#     name = models.CharField(_(u'Назва'), max_length=20)
#     slug = models.SlugField(_(u'Slug auto'), max_length=20, unique=True)
#     order = models.PositiveIntegerField()
#
#     class Meta:
#         ordering = ('order',)
#         verbose_name = _(u'Категорія')
#         verbose_name_plural = _(u'Категорії')
#
#     def __unicode__(self):
#         return self.name


PRODUCT_TYPES = (
    ('goods', _('Товар')),
    ('service', _('Сервіс'))
)

PRODUCT_CONDITIONS = (
    ('new', _('Новий')),
    ('used', _('Б\У')),
    ('waste', _('Зламаний'))
)

NEW_PRODUCT_STATUS_NAME = _('Новий')

STATUS = (
    ('available', _("Новий")),
    ("in_deal", _("В процесі виконання")),
    ("cancel", _("Скасовано  - не актуальна угода")),
)

SWITCH_STATUS = {
    'available': ('booked', 'payed', 'cancel'),
    'booked': ('payed', 'not_relevant', 'cancel'),
    'payed': ('sent', 'cancel', 'cancel_payment'),
    'sent': ('done', 'variance', 'done'),
    'done': (),
    'variance': ('reverse',),
    'reverse': ('reject_get',),
    'cancel': (),
}

SIMILAR_STATUS_LIST = ('available', 'cancel')


class Product(models.Model):
    spay_id = models.CharField(max_length=6,
                               default=get_spay_id,
                               unique=True)
    # category = models.ForeignKey(Category, verbose_name=_(u'Категорія'), related_name='category', blank=True, null=True)
    created_date = models.DateTimeField(_('Дата створення'), auto_now_add=True)
    updated_date = models.DateTimeField(_('Дата зміни'), auto_now=True)
    name = models.CharField(_('Назва'), max_length=60)
    type = models.CharField(_('Тип'), choices=PRODUCT_TYPES, max_length=10, default='goods')
    text = models.TextField(_('Опис'))
    image = ImageField(_('Основне зображення'), upload_to='media/', blank=True)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_(u'Власник товару'))
    slug = models.SlugField(_('Slug auto'), max_length=60, unique=True)
    terms = models.TextField(_('Умови'))
    product_status = models.CharField(default='available', choices=STATUS, max_length=16)
    condition = models.CharField(_('Стан'), choices=PRODUCT_CONDITIONS, max_length=10, default='new')
    is_published = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify("%s-%s" % (self.name, self.spay_id))
        super(Product, self).save(*args, **kwargs)

    def change_product_status(self, new_status):
        if new_status in SWITCH_STATUS.get(self.status):
            self.product_status = new_status
            self.save()

    objects = models.Manager()
    users = ProductQuerySet.as_manager()

    # def get_absolute_url(self):
    #     return reverse('product:details', args=[self.slug])

    def last_transaction(self):
        last_trade = self.trade_set.last()
        return last_trade if last_trade else None

    @property
    def status(self):
        return self.product_status if self.product_status in SIMILAR_STATUS_LIST \
            else self.last_transaction().status

    @property
    def get_status_display(self):
        return self.get_product_status_display() if self.product_status in SIMILAR_STATUS_LIST \
            else self.last_transaction().get_status_display()

    def buyer_transaction(self, user):
        trade = self.trade_set.filter(buyer=user).last()
        return trade if trade else '1323123123'

    @property
    def service_price(self):
        return (self.price * Decimal(0.03)).quantize(Decimal("0.01"))

    @property
    def total_price(self):
        return self.price + self.service_price

    @property
    def list_to_cancel(self):
        return 'available', 'booked', 'payed'

    class Meta:
        verbose_name = _(u'Продук')
        verbose_name_plural = _(u'Продукти')
        ordering = ('-created_date',)

    def __unicode__(self):
        return self.name


class ProductImage(models.Model):
    images = models.ForeignKey(Product, verbose_name=_(u'Зображення'), related_name='images')
    image = ImageField(_(u'Зображення'), upload_to='media/')

    class Meta:
        verbose_name = _(u'Зображення')
        verbose_name_plural = _(u'Зображення')

    def __unicode__(self):
        return str(self.id)
