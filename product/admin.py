from django.contrib import admin
from django.utils.html import format_html
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin
from cards.models import ProductCard, ProductBankAccount
from trade.models import Trade
# from suit.admin import SortableModelAdmin
from .models import Product, ProductImage


# @admin.register(Category)
# class CategoryAdmin(SortableModelAdmin):
#     prepopulated_fields = {'slug': ('name',)}
#     sortable = 'order'
#
#     def has_add_permission(self, request):
#         if Category.objects.count() == 6:
#             return False
#         return True


class ProductImageInline(AdminImageMixin, admin.TabularInline):
    model = ProductImage
    extra = 1
    max_num = 5


class TradeInline(admin.StackedInline):
    model = Trade
    extra = 0
    max_num = 0


class CardInline(admin.TabularInline):
    model = ProductCard
    extra = 0
    max_num = 0


class BankAccountInline(admin.TabularInline):
    model = ProductBankAccount
    extra = 0
    max_num = 0


@admin.register(Product)
class ProductAdmin(AdminImageMixin, admin.ModelAdmin):
    inlines = (TradeInline, BankAccountInline, ProductImageInline)
    prepopulated_fields = {'slug': ('spay_id',)}
    list_display = ('photo_thumbnail', 'spay_id', 'name', 'product_status', 'price', 'user')
    search_fields = ('spay_id', 'name', 'user__email', 'user__name')
    list_display_links = ('photo_thumbnail', 'spay_id', 'name')

    def photo_thumbnail(self, obj):
        im = get_thumbnail(obj.image, '60x60', quality=99)
        if im:
            return format_html(
                '<img src="{}" border="0" alt="" width="{}" height="{}" />',
                im.url, im.width, im.height)

    photo_thumbnail.short_description = 'Photo'
    photo_thumbnail.allow_tags = True
