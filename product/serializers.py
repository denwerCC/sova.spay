from rest_framework import serializers
from models import Product
from suser.serializers import PublibUserSerializer


class ProductSerializer(serializers.ModelSerializer):
    user = PublibUserSerializer(read_only=True)

    def create(self, validated_data):
        product = Product.objects.create(user_id=self.context["request"].user.id, **validated_data)
        return product

    class Meta:
        model = Product
        fields = ('spay_id', 'name', 'text', 'image', 'price', 'user', 'status', 'created_date', 'updated_date')
        read_only_fields = ('spay_id', 'user', 'created_date', 'status', 'updated_date')
