from django.db import models


class ProductQuerySet(models.QuerySet):
    def seller(self, user):
        return self.filter(user=user)

    def all_products(self, user):
        return self.filter(models.Q(user=user) | models.Q(trade__buyer=user)). \
            select_related().order_by('-trade__modified')

    def by_spay_id(self, spay_id):
        return self.filter(spay_id=spay_id)
