# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from trade.models import Trade
from suser.models import USER_TYPES
from .models import Product, ProductImage


class BaseProductForm(forms.ModelForm):
    email = forms.EmailField(label=_("Email продавця"), widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'ajax-url': reverse_lazy('user:exists'),
    }))
    phone = forms.CharField(label=_("Телефон продавця"), required=True, widget=forms.TextInput(attrs={
        'class': 'form-control',
    }))
    user_type = forms.ChoiceField(label=_("Тип користувача"), initial='private', choices=USER_TYPES, required=False,
                                  widget=forms.RadioSelect(attrs={
                                      # 'class': 'form-control',
                                  }))
    password = forms.CharField(label=_("Пароль"), required=False, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
    }))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(BaseProductForm, self).__init__(*args, **kwargs)
        for field_name in self.fields:
            field = self.fields.get(field_name)
            field.widget.attrs.update(
                {'placeholder': field.label}
            )
            field.label = ' '
        if self.user.is_authenticated():
            self.fields["email"].initial = self.user.email
            self.fields['email'].widget.attrs['readonly'] = True
            self.fields["phone"].initial = self.user.phone

    def clean_email(self):
        email = self.cleaned_data['email']
        if get_user_model().objects.filter(email=email).exists() and not self.user.is_authenticated():
            raise forms.ValidationError(_("Користувач з таким email вже зареєстрований і має залогінитись"))
        return email

    class Meta:
        model = Product
        fields = ('name', 'text', 'type', 'image', 'price', 'terms', 'condition')


class BuyProductForm(BaseProductForm):
    BUY_STATUS = "payed"

    def __init__(self, *args, **kwargs):
        super(BuyProductForm, self).__init__(*args, **kwargs)
        self.fields['product'].widget = forms.HiddenInput()

    def clean(self):
        data = super(BuyProductForm, self).clean()
        if "toBuy" in self.data:
            self.instance.status = self.BUY_STATUS
        return data

    class Meta:
        model = Trade
        fields = ('product',)


class CreateProductForm(BaseProductForm):
    email_buyer = forms.EmailField(label=_("Email покупця"), required=False, widget=forms.EmailInput(attrs={
        'class': 'form-control',
    }))
    phone_buyer = forms.CharField(label=_("Телефон покупця"), required=False, widget=forms.TextInput(attrs={
        'class': 'form-control',
    }))

    def __init__(self, *args, **kwargs):
        super(CreateProductForm, self).__init__(*args, **kwargs)
        self.fields['type'].widget = forms.HiddenInput()


class UpdateProductForm(forms.ModelForm):
    image = forms.ImageField(required=False,
                             error_messages={'invalid': _("Image files only")},
                             widget=forms.FileInput)

    def __init__(self, *args, **kwargs):
        super(UpdateProductForm, self).__init__(*args, **kwargs)
        for field_name in self.fields:
            field = self.fields.get(field_name)
            field.widget.attrs.update(
                {'placeholder': field.label}
            )
            field.label = ' '
        self.fields["type"].widget.attrs['class'] = "hidden"

    class Meta:
        model = Product
        fields = ('name', 'text', 'image', 'price', 'terms', 'type', 'condition')


class ProductImageForm(forms.ModelForm):
    image = forms.ImageField(required=False,
                             error_messages={'invalid': _("Image files only")},
                             widget=forms.FileInput)

    class Meta:
        model = ProductImage
        fields = ('image',)
