from django.http import Http404
from django.views.generic import DetailView, ListView, CreateView, View
from django.contrib.auth import get_user_model, login, authenticate
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect
from extra_views import InlineFormSet, CreateWithInlinesView, UpdateWithInlinesView
from core.models import Terms
from trade.models import Trade
from .models import Product, ProductImage
from .forms import BuyProductForm, CreateProductForm, UpdateProductForm, ProductImageForm


class ImagesInline(InlineFormSet):
    model = ProductImage
    form_class = ProductImageForm
    fields = ('image',)
    extra = 4
    max_num = 4


class SpayLogicMixin(object):
    def get_form_kwargs(self):
        kwargs = super(SpayLogicMixin, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def _form_valid(self, form):
        if self.request.user.is_authenticated():
            setattr(form.instance, self.form_user_attr_name, self.request.user)
        else:
            user, password = get_user_model().objects.create_user_shortcut(
                form.cleaned_data.get('email'),
                form.cleaned_data.get('phone'),
                type=form.cleaned_data.get('user_type')
            )
            setattr(form.instance, self.form_user_attr_name, user)
            user = authenticate(email=user.email, password=password)
            if user is not None:
                login(self.request, user)

    def forms_valid(self, form, inlines):
        self._form_valid(form)
        return super(SpayLogicMixin, self).forms_valid(form, inlines)

    def form_valid(self, form):
        self._form_valid(form)
        return super(SpayLogicMixin, self).form_valid(form)


class ProductCreateView(SpayLogicMixin, CreateWithInlinesView):
    template_name = "product/create_product.html"
    model = Product
    form_class = CreateProductForm
    context_object_name = 'product'
    form_user_attr_name = 'user'
    inlines = [ImagesInline]

    def get_initial(self):
        if self.kwargs.get('price'):
            return {
                'price': self.kwargs.get('price'),
                'type': self.kwargs.get('type'),
            }
        return super(ProductCreateView, self).get_initial()

    def get_context_data(self, **kwargs):
        context = super(ProductCreateView, self).get_context_data(**kwargs)
        context['terms'] = Terms.objects.first()
        return context

    def get_success_url(self):
        return reverse('card:add', args=[self.object.slug])


class ProductDetailViewBySpay(SpayLogicMixin, CreateView):
    template_name = "product/product_buy.html"
    slug_field = "spay_id"
    form_class = BuyProductForm
    model = Trade
    product = Product.objects.none()
    success_url = 'user/profile/'
    form_user_attr_name = 'buyer'

    def get_initial(self):
        initial = super(ProductDetailViewBySpay, self).get_initial()
        try:
            initial['product'] = Product.objects.get(spay_id=self.kwargs[self.slug_field])
        except Product.DoesNotExist:
            raise Http404
        return initial

    def get_context_data(self, **kwargs):
        context = super(ProductDetailViewBySpay, self).get_context_data(**kwargs)
        context['product'] = self.get_initial().get('product')
        return context

    def get_success_url(self):
        if self.request.user.type == "private" and \
                        self.object.status == self.form_class.BUY_STATUS:
            return "https://www.liqpay.com/"
        else:
            return reverse('trade:list')


class UsersObjectMixin(object):
    def get_object(self, queryset=None):
        queryset = self.model.objects.filter(user=self.request.user)
        return super(UsersObjectMixin, self).get_object(queryset)

    def get_queryset(self):
        queryset = super(UsersObjectMixin, self).get_queryset()
        return queryset.filter(user=self.request.user)


class UsersProductsView(ListView, UsersObjectMixin):
    template_name = "product_list.html"
    model = Product


class ProductUpdateView(LoginRequiredMixin, UpdateWithInlinesView):
    template_name = "product/product_update.html"
    model = Product
    form_class = UpdateProductForm
    context_object_name = 'product'
    inlines = [ImagesInline]

    def get_success_url(self):
        return reverse('trade:update_as_seller', args=(self.object.spay_id,))

    def get_object(self, queryset=None):
        return super(ProductUpdateView, self).get_object(
            self.model.users.seller(self.request.user).filter(product_status="available"))


# class ProductPostCreateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
#     template_name = "create_product.html"
#     model = Product
#     context_object_name = 'product'
#     fields = ('name', 'text', 'image', 'price')
#
#     def form_valid(self, form):
#         form.instance.user = self.request.user
#         form.instance.is_published = True
#         return super(ProductPostCreateView, self).form_valid(form)
#
#     def test_func(self):
#         return self.get_object().is_published is False


# class ProductDetailView(DetailView):
#     template_name = "product/product_change_status_seller.html"
#     model = Product
#     context_object_name = 'product'
#     buttons_template_name = "status/seller/available.html"


def change_product_status(request, spay_id, new_status):
    product = get_object_or_404(Product, spay_id=spay_id, user=request.user)
    product.change_product_status(new_status)
    return product


@login_required
def remove_product(request, spay_id, status='cancel'):
    product = change_product_status(request, spay_id, status)
    if product.last_transaction():
        product.last_transaction().change_trade_status(status)
    return redirect("trade:list")
