from django.conf.urls import url
from .views import ProductCreateView, ProductDetailViewBySpay, UsersProductsView, ProductUpdateView, remove_product

urlpatterns = [
    # url(r'^new/$', ProductCreateView.as_view(), name='create'),
    url(r'^new/(?P<type>[-\w]+)/(?P<price>[-\w]+)/$', ProductCreateView.as_view(), name='create_as'),
    url(r'^update/(?P<slug>[-\w]+)/$', ProductUpdateView.as_view(), name='update'),

    url(r'^spay/(?P<spay_id>[\-\w]+)/$', ProductDetailViewBySpay.as_view(), name='details_by_spay_id'),

    url(r'^remove/(?P<spay_id>[\-\w]+)/$', remove_product, name='remove'),

    url(r'^user/all/$', UsersProductsView.as_view(), name='user_list'),
]
