from django import template
from django.core.urlresolvers import reverse

register = template.Library()


@register.simple_tag
def get_user_trade_url(product, user):
    if product.user == user:
        return reverse('trade:update_as_seller', args=[product.spay_id])
    else:
        return reverse('trade:update_as_buyer', args=[product.buyer_transaction(user).transaction_id])
