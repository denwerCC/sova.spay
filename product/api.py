from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView
from serializers import ProductSerializer
from .models import Product
from rest_framework import viewsets
from rest_framework import permissions


class MyUserPermissions(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user == obj.user


class ProductDetailsBySpayApi(RetrieveAPIView):
    queryset = Product.objects
    lookup_field = "spay_id"
    lookup_url_kwarg = "spay_id"
    serializer_class = ProductSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    http_method_names = ['get']


class ProductViewSetApi(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects
    permission_classes = [
        MyUserPermissions
    ]