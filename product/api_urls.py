from django.conf.urls import url
from .api import ProductDetailsBySpayApi, ProductViewSetApi
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'actions', ProductViewSetApi)
urlpatterns = [
                  url(r'^spay/(?P<spay_id>[\-\w]+)/$', ProductDetailsBySpayApi.as_view(),
                      name='api_details_by_spay_id'),

              ] + router.urls
