from __future__ import unicode_literals
from django.db import models


class Terms(models.Model):
    legal_entity = models.TextField()
    physical_entity = models.TextField()
