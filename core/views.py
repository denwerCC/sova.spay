# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.views.generic import TemplateView
from product.models import Product
from django.http import JsonResponse


def index_view(request):
    if request.method == 'POST':
        if request.POST.get('action') == "sell":
            return JsonResponse(
                {'status': 'OK', 'url': reverse("product:create_as", kwargs={"type": request.POST.get('type'),
                                                                             "price": request.POST.get('price')})})
        else:
            if request.POST.get('spay_id'):
                current_product = Product.users.by_spay_id(request.POST.get('spay_id'))
                if not current_product.exists():
                    return JsonResponse({'status': 'error', 'text': _('Товар з таким Spay ID не існує')})
                elif current_product[0].status == 'done':
                    return JsonResponse({'status': 'sold', 'text': _('Товар з таким Spay ID вже проданий')})
                elif current_product[0].status == 'cancel':
                    return JsonResponse({'status': 'sold', 'text': _('Товар з таким Spay ID видалений')})
                else:
                    return JsonResponse({'status': 'OK', 'url': reverse("product:details_by_spay_id",
                                                                        args=[request.POST.get('spay_id')])})
            else:
                return render(request, 'index.html')
    return render(request, 'index.html')


class IndexView(TemplateView):
    template_name = "index-shop.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data()
        return context
