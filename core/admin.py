from django.contrib import admin
from .models import Terms


@admin.register(Terms)
class TermsAdmin(admin.ModelAdmin):
    list_display = ('physical_entity', 'legal_entity')

# Register your models here.
