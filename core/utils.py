from django.core.mail import send_mail


def send_email_consumer(message):
    payload = message.content['payload']
    send_mail(payload['subject'], payload['body'], 'uspay@byteside.co', payload['email'])
