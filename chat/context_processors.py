from .models import Theme


def messages_count(request):
    if not request.user.is_anonymous():
        return {"messages_count": Theme.user_messages.new_from_all_users_count(request.user)}
    else:
        return {}
