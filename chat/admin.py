from django.contrib import admin
from .models import Message, Theme


@admin.register(Message)
class ConversationAdmin(admin.ModelAdmin):
    list_display = ('theme', 'text', 'author')


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    list_display = ('product', 'buyer')
