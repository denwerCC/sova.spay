from django.db.models import QuerySet, Q
from django.db.models import Count, Case, When, IntegerField


class UsersQuerySet(QuerySet):
    def for_user(self, current_user):
        return self.filter(
            Q(product__user=current_user) |
            Q(buyer=current_user)
        ).select_related()

    def for_user_with_count(self, current_user):
        return self.for_user(current_user).annotate(num_new_messages=Count(
            Case(
                When(Q(messages__is_read=False) & ~Q(messages__author=current_user), then=1),
                output_field=IntegerField(),
            )
        )).order_by('-num_new_messages')

    def new_from_all_users(self, current_user):
        return self.for_user(current_user).filter(
            Q(messages__is_read=False) & ~Q(messages__author=current_user)
        )

    def new_from_user(self, current_user, user):
        return self.filter(
            buyer=current_user,
            product__user=user,
            messages__is_read=False
        ).exclude(
            messages__author=current_user
        ).select_related()

    def new_from_all_users_count(self, current_user):
        return self.new_from_all_users(current_user).count()

    def read_all(self, current_user):
        self.new_from_all_users(current_user).update(is_read=True)
