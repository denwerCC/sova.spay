from django.conf.urls import url
from .views import UserConversationList, UserConversationMessageList, create_chat_with_buyer, create_chat_with_seller

urlpatterns = [
    url(r'^seller/(?P<spay_id>[\-\w]+)/$', create_chat_with_seller, name='contact_seller'),
    url(r'^buyer/(?P<transaction_id>[^/]+)/$', create_chat_with_buyer, name='contact_buyer'),
    url(r'^list/$', UserConversationList.as_view(), name='list'),
    url(r'^message_list/(?P<pk>[\-\w]+)/$', UserConversationMessageList.as_view(), name='message_list'),
]
