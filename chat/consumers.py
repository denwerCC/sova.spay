from __future__ import unicode_literals
import json
import logging
from channels import Channel, Group
from channels.auth import http_session_user, channel_session_user, channel_session_user_from_http
from .models import Message, Theme

log = logging.getLogger(__name__)


def msg_consumer(message):
    msg = Message.objects.create(
        author_id=message.content['author'],
        theme_id=message.content['theme']['pk'],
        text=message.content['message'],
    )
    Group("new-messages-%s" % msg.recipient.pk).send({"text": json.dumps(
        msg.as_dict())
    })


@channel_session_user_from_http
def ws_add(message, product, user_id):
    theme, created = Theme.objects.get_or_create(product_id=product,
                                                 buyer_id=user_id)
    message.channel_session['theme'] = {"pk": theme.pk, "name": theme.get_chat_name}
    Group("%s" % theme.get_chat_name).add(message.reply_channel)


@channel_session_user_from_http
def ws_new_message_count(message):
    Group("new-messages-%s" % message.user.pk).add(message.reply_channel)


@channel_session_user
def ws_message(message):
    Channel("chat-messages").send({
        "author": message.user.pk,
        "theme": message.channel_session['theme'],
        "message": message['text'],
    })


@channel_session_user
def ws_disconnect(message, product, user_id):
    Group(message.channel_session['theme']['name']).discard(message.reply_channel)


@channel_session_user
def ws_new_message_count_disconnect(message):
    Group("new-messages-%s" % message.user.pk).discard(message.reply_channel)
