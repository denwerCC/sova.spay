from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.views.generic import ListView, DetailView
from django.shortcuts import get_object_or_404, redirect
from .models import Message, Theme
from product.models import Product
from trade.models import Trade


@login_required
def create_chat_with_seller(request, spay_id):
    product = get_object_or_404(Product, spay_id=spay_id)
    theme, created = Theme.objects.get_or_create(product=product,
                                                 buyer=request.user)
    return redirect('chat:message_list', pk=theme.id)


@login_required
def create_chat_with_buyer(request, transaction_id):
    trade = get_object_or_404(Trade,
                              transaction_id=transaction_id,
                              product__user=request.user)
    theme, created = Theme.objects.get_or_create(product=trade.product,
                                                 buyer=trade.buyer)
    return redirect('chat:message_list', pk=theme.id)


class UserConversationList(LoginRequiredMixin, ListView):
    template_name = "chat/list.html"
    model = Theme

    def get_queryset(self):
        return self.model.user_messages.for_user_with_count(self.request.user)


class UserConversationMessageList(LoginRequiredMixin, DetailView):
    template_name = "chat/message_list.html"
    model = Theme

    def get_queryset(self):
        queryset = super(UserConversationMessageList, self).get_queryset()
        return queryset.filter(
            Q(product__user=self.request.user) |
            Q(buyer=self.request.user)
        )

    def get_context_data(self, **kwargs):
        context = super(UserConversationMessageList, self).get_context_data(**kwargs)
        self.object.read_theme_messages(self.request.user)
        context["messages"] = Message.objects.filter(theme=self.object)
        return context
