from __future__ import unicode_literals
import json
from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from product.models import Product
from .managers import UsersQuerySet
from django.template.loader import render_to_string
from channels import Group


class Theme(models.Model):
    product = models.ForeignKey(Product)
    buyer = models.ForeignKey(settings.AUTH_USER_MODEL)

    @property
    def get_chat_name(self):
        return "%s-%s" % (self.product.slug, self.buyer.pk)

    @property
    def get_theme_div_id(self):
        return "theme-%s" % self.pk

    def read_theme_messages(self, current_user):
        return self.messages.filter(is_read=False). \
            exclude(author=current_user).update(is_read=True)

    objects = models.Manager()
    user_messages = UsersQuerySet.as_manager()


class Message(models.Model):
    theme = models.ForeignKey(Theme, related_name="messages")
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    created_date = models.DateTimeField(auto_now_add=True)
    read_date = models.DateTimeField(auto_now=True)
    text = models.TextField()
    is_read = models.BooleanField(default=False)

    @property
    def recipient(self):
        return self.theme.buyer if self.theme.buyer != self.author else self.theme.product.user

    def get_theme_url(self):
        return reverse("chat:message_list", args=[self.theme_id])

    def as_dict(self):
        return {"theme": self.theme.get_theme_div_id, "author": self.author.name_to_display,
                "text": self.text, 'url': self.get_theme_url()}

    def save(self, *args, **kwargs):
        super(Message, self).save(*args, **kwargs)
        Group("%s" % self.theme.get_chat_name).send({
            "text": json.dumps({"message": self.text,
                                "author": self.author.pk,
                                "html": render_to_string('chat/message_item.html',
                                                         {"message": self})})
        })

    def __unicode__(self):
        return "From {from_user} message {text} at {created_date}".format(
            from_user=self.author,
            text=self.text[:20],
            created_date=self.created_date
        )

    class Meta:
        ordering = ('created_date',)
